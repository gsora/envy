package envy

import (
	"errors"
	"fmt"
	"os"
	"reflect"
	"strconv"
	"strings"
	"unicode"
)

const (
	tagName = "envy"

	varNameTagField      = "var_name"
	defaultValueTagField = "default_value"
	requiredTagField     = "required"

	// NoPrefix is a convenience const used to specify no prefix applied to environment variables read.
	NoPrefix = ""
)

type parsingOptions struct {
	VarName      string
	DefaultValue string
	Required     bool
}

func splitCapitalized(s string) (words []string) {
	if len(s) <= 0 {
		return
	}

	if unicode.IsLower(rune(s[0])) {
		return
	}

	var word string
	for i, c := range s {
		if unicode.IsUpper(c) {
			if i != 0 {
				words = append(words, word)
				word = ""
			}
		}

		word += strings.ToLower(string(c))
	}

	words = append(words, word)
	return
}

func toEnvVarString(s, prefix string) string {
	sc := splitCapitalized(s)
	if sc == nil {
		return ""
	}

	if prefix != "" {
		sc = append([]string{prefix}, sc...)
	}

	for i := 0; i < len(sc); i++ {
		sc[i] = strings.ToUpper(sc[i])
	}

	return strings.Join(sc, "_")
}

func parseTags(fieldName, prefix, t string) parsingOptions {
	var po parsingOptions

	// remove any whitespace
	t = strings.Replace(t, " ", "", -1)

	po.VarName = toEnvVarString(fieldName, prefix)

	tt := strings.Split(t, ",")
	if len(tt) == 1 && tt[0] == "" {
		return po
	}

	for _, ttt := range tt {
		ts := strings.Split(ttt, "=")
		switch {
		case strings.HasPrefix(ts[0], varNameTagField):
			if len(ts) < 2 {
				continue // only "var_name" specified, not a real value
			}
			po.VarName = ts[1]
		case strings.HasPrefix(ts[0], defaultValueTagField):
			if len(ts) < 2 {
				continue // only "default_value" specified, not a real value
			}
			po.DefaultValue = ts[1]
		case strings.HasPrefix(ts[0], requiredTagField):
			po.Required = true
		}
	}

	return po
}

func get(envName string) string {
	return os.Getenv(envName)
}

func setValue(v reflect.Value, data string) {
	switch v.Kind() {
	case reflect.Bool:
		db, _ := strconv.ParseBool(data)
		v.SetBool(db)
	case reflect.String:
		v.SetString(data)
	case reflect.Int, reflect.Int16, reflect.Int32, reflect.Int64, reflect.Int8:
		db, _ := strconv.ParseInt(data, 10, 64)
		v.SetInt(db)
	case reflect.Uint, reflect.Uint64, reflect.Uint16, reflect.Uint32, reflect.Uint8, reflect.Uintptr:
		db, _ := strconv.ParseUint(data, 10, 64)
		v.SetUint(db)
	case reflect.Float64, reflect.Float32:
		db, _ := strconv.ParseFloat(data, 64)
		v.SetFloat(db)
	}
}

// Read fills dest with environment variables content based on the struct's envy tags.
func Read(dest interface{}, prefix string) error {
	fields := reflect.TypeOf(dest)
	values := reflect.ValueOf(dest).Elem()

	if fields == nil {
		return errors.New("argument is nil")
	}

	for i := 0; i < fields.Elem().NumField(); i++ {
		field := fields.Elem().Field(i)
		value := values.Field(i)

		tag := field.Tag.Get(tagName)

		if tag == "" {
			continue // field has no tag
		}

		pt := parseTags(field.Name, prefix, tag)

		associatedEv := get(pt.VarName)
		if associatedEv == "" {
			if pt.DefaultValue != "" {
				associatedEv = pt.DefaultValue
			} else if pt.Required {
				return fmt.Errorf("environment variable %s required but not set", pt.VarName)
			}
		}

		setValue(value, associatedEv)
	}

	return nil
}
