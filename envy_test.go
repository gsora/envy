package envy

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_splitCapitalized(t *testing.T) {
	tests := []struct {
		name      string
		s         string
		wantWords []string
	}{
		{
			"empty string returns empty slice",
			"",
			nil,
		},
		{
			"a capitalized, exported string",
			"ExportedString",
			[]string{
				"exported",
				"string",
			},
		},
		{
			"a capitalized, unexported string",
			"unexportedString",
			nil,
		},
		{
			"a uncapitalized, unexported string",
			"nocapitals",
			nil,
		},
		{
			"a single capitalized, exported string",
			"Singlecapital",
			[]string{
				"singlecapital",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			require.Equal(t, tt.wantWords, splitCapitalized(tt.s))
		})
	}
}

func Test_toEnvVarString(t *testing.T) {
	tests := []struct {
		name   string
		s      string
		prefix string
		want   string
	}{
		{
			"empty string",
			"",
			"",
			"",
		},
		{
			"exported, capitalized string without prefix",
			"ExportedString",
			"",
			"EXPORTED_STRING",
		},
		{
			"exported, capitalized string with prefix",
			"ExportedString",
			"PREFIX",
			"PREFIX_EXPORTED_STRING",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			require.Equal(t, tt.want, toEnvVarString(tt.s, tt.prefix))
		})
	}
}

func Test_parseTags(t *testing.T) {
	tests := []struct {
		name      string
		fieldName string
		prefix    string
		tag       string
		want      parsingOptions
	}{
		{
			"empty tags",
			"FieldName",
			"",
			"",
			parsingOptions{
				VarName: "FIELD_NAME",
			},
		},
		{
			"field name with prefix",
			"FieldName",
			"prefix",
			"",
			parsingOptions{
				VarName: "PREFIX_FIELD_NAME",
			},
		},
		{
			"required tag",
			"FieldName",
			"",
			"required",
			parsingOptions{
				VarName:  "FIELD_NAME",
				Required: true,
			},
		},
		{
			"default_value tag",
			"FieldName",
			"",
			"default_value=default",
			parsingOptions{
				VarName:      "FIELD_NAME",
				DefaultValue: "default",
			},
		},
		{
			"default_value tag but no content",
			"FieldName",
			"",
			"default_value",
			parsingOptions{
				VarName: "FIELD_NAME",
			},
		},
		{
			"custom var_name",
			"FieldName",
			"",
			"var_name=custom",
			parsingOptions{
				VarName: "custom",
			},
		},
		{
			"custom var_name, prefix ignored",
			"FieldName",
			"prefix",
			"var_name=custom",
			parsingOptions{
				VarName: "custom",
			},
		},
		{
			"all three fields to non-default values",
			"FieldName",
			"",
			"var_name=custom,required,default_value=works",
			parsingOptions{
				VarName:      "custom",
				Required:     true,
				DefaultValue: "works",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			require.Equal(t, tt.want, parseTags(tt.fieldName, tt.prefix, tt.tag))
		})
	}
}

func TestRead_structWithRequiredTagDefined(t *testing.T) {
	os.Clearenv()

	type tt struct {
		A string `envy:"required"`
	}

	prefix := ""

	env := map[string]string{
		"A": "A",
	}

	s := tt{}

	want := tt{
		A: "A",
	}

	for k, v := range env {
		require.NoError(t, os.Setenv(k, v))
	}

	require.NoError(t, Read((&s), prefix))
	require.Equal(t, s, want)

}

func TestRead_structWithRequiredTagNotDefined(t *testing.T) {
	os.Clearenv()

	type tt struct {
		A string `envy:"required"`
	}

	prefix := ""

	env := map[string]string{}

	s := tt{}

	want := tt{}

	for k, v := range env {
		require.NoError(t, os.Setenv(k, v))
	}

	require.Error(t, Read(&s, prefix))
	require.Equal(t, s, want)

}

func TestRead_structWithRequiredTagNotDefinedButWithDefaultValue(t *testing.T) {
	os.Clearenv()

	type tt struct {
		A string `envy:"required,default_value=default"`
	}

	prefix := ""

	env := map[string]string{}

	s := tt{}

	want := tt{
		A: "default",
	}

	for k, v := range env {
		require.NoError(t, os.Setenv(k, v))
	}

	require.NoError(t, Read((&s), prefix))
	require.Equal(t, s, want)

}

func TestRead_structWithRequiredTagDefinedWithDifferentEnvVarNameAndDefaultValue(t *testing.T) {
	os.Clearenv()

	type tt struct {
		A string `envy:"required,default_value=default,var_name=otherA"`
	}

	prefix := ""

	env := map[string]string{
		"otherA": "A",
	}

	s := tt{}

	want := tt{
		A: "A",
	}

	for k, v := range env {
		require.NoError(t, os.Setenv(k, v))
	}

	require.NoError(t, Read((&s), prefix))
	require.Equal(t, s, want)

}

func TestRead_structWithRequiredTagDefinedAndPrefix(t *testing.T) {
	os.Clearenv()

	type tt struct {
		A string `envy:"required"`
	}

	prefix := "prefixA"

	env := map[string]string{
		"PREFIXA_A": "A",
	}

	s := tt{}

	want := tt{
		A: "A",
	}

	for k, v := range env {
		require.NoError(t, os.Setenv(k, v))
	}

	require.NoError(t, Read((&s), prefix))
	require.Equal(t, s, want)

}

func TestRead_structWithNoTags(t *testing.T) {
	os.Clearenv()

	type tt struct {
		A string
	}

	prefix := ""

	env := map[string]string{
		"A": "A",
	}

	s := tt{}

	want := tt{}

	for k, v := range env {
		require.NoError(t, os.Setenv(k, v))
	}

	require.NoError(t, Read((&s), prefix))
	require.Equal(t, s, want)

}

func TestRead_structWithRequiredTagDefinedAndNoEnvVarAssociated(t *testing.T) {
	os.Clearenv()

	type tt struct {
		A string `envy:"required"`
	}

	prefix := ""

	env := map[string]string{}

	s := tt{}

	want := tt{}

	for k, v := range env {
		require.NoError(t, os.Setenv(k, v))
	}

	require.Error(t, Read((&s), prefix))
	require.Equal(t, s, want)
}

func TestRead_structWithNoParametersWithNoCorrespondingEnvVar(t *testing.T) {
	os.Clearenv()

	type tt struct {
		A string `envy:""`
	}

	prefix := ""

	env := map[string]string{}

	s := tt{}

	want := tt{}

	for k, v := range env {
		require.NoError(t, os.Setenv(k, v))
	}

	require.NoError(t, Read((&s), prefix))
	require.Equal(t, s, want)
}
