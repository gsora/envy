# envy

Bare-bone environment variable-based struct tagging

## Tags

envy supports the following tags: 

| Tag | Meaning | Required | Accepts parameter |
| --- | ------- | -------- | ----------------- |
| `required` | the corresponding environment variable must be defined, otherwised an error will be thrown | no | no |
| `var_name` | the corresponding field will contain the value of the associated to the environment variable named after the parameter | no | yes |
| `default_value` | if the corresponding environment variable is not defined, the associated field will contain the value in parameter | no | yes| 

Users can specify a prefix in the `Read()` function second parameter, which will be placed before the environment variable name derived by the field name.

The prefix will be **ignored** if `var_name` is defined.

## How-to

Add `envy` struct tags to the fields you want to fill with data coming from the environment variables:

```go
package main

import (
    "log"
    "os"
    "gitlab.com/gsora/envy"
)

type MyStruct struct {
    Field   string `envy:"required"`
    Integer int `envy:"required,default_value=42"`
}

func setEnv() {
    os.Setenv("FIELD", "field content")
}

func main()  {
    setEnv()

    var ms MyStruct
    err := envy.Read(&ms, envy.NoPrefix)
    if err != nil {
        log.Fatal(err)
    }
    
    log.Printf("field = %s, integer = %d\n", ms.Field, ms.Integer)
}
```

